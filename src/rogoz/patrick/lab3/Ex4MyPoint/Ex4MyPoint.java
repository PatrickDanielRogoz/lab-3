package rogoz.patrick.lab3.MyPoint;
import java.util.Scanner;

public class MyPoint {
	
    int x;
    int y;
    
    public MyPoint()
    {
        x=0;
        y=0;
    }
    
    public MyPoint(int a, int b)
    {
        x=a;
        y=b;
    }
    
    int Getx()
    {
        return x;
    }
    
    int Gety()
    {
        return y;
    }
    
    void Setx(int m)
    {
        x=m;
        System.err.println(x);
    }
    
    void Sety(int n)
    {
        y=n;
        System.err.println(y);
    }
    
    String tostring()
    {
        String str1 = Integer.toString(x);
        String str2 = Integer.toString(y);
        return new StringBuilder().append("(").append(x).append(",").append(y).append(")").toString();


    }
    
    double distance(int a, int b )
    { double d;
        d=Math.sqrt((a-x)*(a-x)+(b-y)*(b-y));
        return d;
    }
    
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int a=in.nextInt();
        int b=in.nextInt();
        MyPoint p1=new MyPoint();
        MyPoint p2=new MyPoint(a,b);
        String s1;
        s1=p1.tostring();
        System.err.print(s1);
        String s2;
        s2=p2.tostring();
        System.err.print(s2);
        int m=in.nextInt();
        int n=in.nextInt();
        double d;
        d=p1.distance(m,n);
        System.err.print(d);
    }

}
