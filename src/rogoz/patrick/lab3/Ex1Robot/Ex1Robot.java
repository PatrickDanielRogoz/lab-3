package rogoz.patrick.lab3.Robot;
import java.util.Scanner;

public class Robot {
    
	int poz;
	
    public Robot(int x )
    {
        poz=x;
    }
    
    void move ()
    {
        poz=1;
        System.err.println("Se misca. [poz="+poz+"]");
    }
    
    void move (int k)
    {
        if(k>=1) poz=k;
        System.err.println("Se misca. [poz="+poz+"]");
    }
    
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        int x = in.nextInt();
        int k = in.nextInt();
        Robot r1= new Robot(x);
        r1.move();
        r1.move(k);
    }
}
