package rogoz.patrick.lab3.Cerc;
import java.util.Scanner;

public class Cerc {
    
	double raza;
    String cerc;
    
    public Cerc()
    {
        raza=0.1;
        cerc="red";
    }
    
    public Cerc(double a, String l)
    {
        raza=a;
        cerc=l;
    }
    
    void getraza()
    {
        System.err.println(raza);
    }
    
    void getArea()
    {
        double a;
        a= 3.14*(raza*raza);
        System.err.println(a);
    }
    
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        double x = in.nextDouble();
        String s= in.next();

        Cerc r1 = new Cerc();
        Cerc r2 = new Cerc(x,s);
        r2.getraza();
    }
}
